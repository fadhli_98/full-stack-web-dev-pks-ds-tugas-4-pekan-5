<?php

namespace App;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $table = 'post';
    protected $fillable = ['comment'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot(){
        parent::boot();

        static::creating( function($model){
            if (!($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = str::uuid();

            

            }

            
            
        });
    }
}
